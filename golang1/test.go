package main

import (
	"fmt"

	"github.com/gdexlab/go-render/render"
)

type Todo struct {
	title      int
	isComplete bool
}

func main() {

	todo := []*Todo{}

	for i := 1; i < 10; i++ {
		obj := Todo{
			title:      i,
			isComplete: false,
		}
		todo = append(todo, &obj)
	}
	listdate(todo)
	fmt.Println("**add")
	for i := 2; i < 7; i++ {
		for x, data := range todo {
			if data.title == i {
				obJdata := Todo{
					title:      i,
					isComplete: true,
				}
				todo[x] = &obJdata
			}
		}
	}

	listdate(todo)
	fmt.Println("**update")
	for i := 1; i < 1; i++ {
		for i, data := range todo {
			if data.title == i {
				todo = append(todo[:i], todo[i+1:]...)
			}
		}

	}
	listdate(todo)
	fmt.Println("**delete")
}

func listdate(tt []*Todo) {
	for _, date := range tt {
		output := render.AsCode(date)
		fmt.Println(output)

	}
}
